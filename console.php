<?php

require __DIR__.'/vendor/autoload.php';

use App\Commands\CountByCommand;
use Symfony\Component\Console\Application;

$application = new Application();

// ... register commands
$application->add(new CountByCommand());

$application->run();
