## About
This is PHP CLI App Academy task implementation.

Project built using **symfony** framework.

Project has:
- Implemented `ReaderInterface` `OfferCollectionInterface` `OfferInterface`
- Implemented `Factory` pattern
- Implemented `Decorator` pattern
- Custom CLI command `app:task`
- Unit tests
- Logging
- SOLID principles
- Local database (JSON file)

## Installation
1. Clone project:<br>
`git clone https://gitlab.com/AzuolasSlipavicius/academy-task.git`
2. Navigate to project:<br>
`cd academy-task`
3. Install composer:<br>
`composer install`

## Usage
Command `app:task` expects at least 2 arguments `count-by` `value-from` and
third argument `value-to` is optional.

###### Arguments usage
If you pass only 2 arguments:
- App will count all matched results from _JSON_ where **key** `count-by` has **value** `value-from
`

If you pass third argument:
- App will count all matched results between given rage `value-from
` and `value-to
`

###### Options

By default application is using local database (_JSON file_).
You can pass new JSON, XML, CSV file or remote URL to application by adding 2 options (**both required**):

`--source=path_to_file`<br>
`--source-format=json`


###### Help

Use command to get all info<br>
`php bin/console app:task --help`

## Examples
`php bin/console app:task price 10` => output will be total matches where `price` = `10`

`php bin/console app:task price 10 500` => output will be total matches count in given range

## Tests
To run tests:<br>
`php bin/phpunit`

## Local JSON file
Local JSON file has these values:

| Name | Value from  | Value to | 
| ---      | ---      | ---      |
| offerId   | 1   | 1000 |
| productTitle   | string   | string |
| vendorId | 20 |25|
| price   |      10.00    | 500.00   |

## Mocking application
To generate fake data I suggest use https://www.mockaroo.com/

To get free end point for App testing I suggest use https://mocki.io/fake-json-api