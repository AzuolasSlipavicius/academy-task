<?php
declare(strict_types=1);

namespace App\Tests\Task;

use App\Task\Reader\Factory;
use App\Task\Reader\JsonReader;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

class FactoryTest extends TestCase
{
    private Factory $factory;

    protected function setUp(): void
    {
        $this->factory = new Factory();
    }

    public function testCorrectReader():void
    {
        $reader = $this->factory->create('json');
        $this->assertInstanceOf(JsonReader::class, $reader);

    }

    public function testWrongFileMimeException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->factory->create(format: 'test-file-mime');
    }

}