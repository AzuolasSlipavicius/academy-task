<?php
declare(strict_types=1);

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CountByCommandTest extends KernelTestCase
{
    private CommandTester $commandTester;
    private string $countBy = 'testPrice';
    private string $valueFrom = '200';
    private string $valueTo = '400';
    private ?string $source = __DIR__ . '/../data/test.json';
    private ?string $sourceFormat = 'json';

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('app:task');
        $this->commandTester = new CommandTester($command);
    }

    /**
     * Build up command arguments and options and execute them.
     */
    private function testerCommandExecute(): void
    {
        $params = [
            'count-by' => $this->countBy,
            'value-from' => $this->valueFrom,
            'value-to' => $this->valueTo,
        ];

        if (isset($this->source)) {
            $params['--source'] = $this->source;
        }

        if (isset($this->sourceFormat)) {
            $params['--source-format'] = $this->sourceFormat;
        }

        $this->commandTester->execute($params);
    }

    public function testExecuteIsSuccessful(): void
    {
        $this->testerCommandExecute();

        $this->commandTester->assertCommandIsSuccessful();
    }

    public function testCorrectOutput(): void
    {
        $this->testerCommandExecute();

        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString('2', $output);
    }

    public function testWrongArgumentType(): void
    {
        $this->valueFrom = 'test';
        $this->testerCommandExecute();

        $needle = sprintf('Value-from should be int, \'%s\' given.', $this->valueFrom);

        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString($needle, $output);

    }

    public function testExpectsAdditionOption(): void
    {
        $this->sourceFormat = null;
        $this->testerCommandExecute();

        $needle = 'Please pass  --source-format  option';

        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString($needle, $output);

    }

    public function testWrongSourceFormatOption(): void
    {
        $this->sourceFormat = 'testFormat';
        $this->testerCommandExecute();

        $needle = sprintf('Format \'%s\' not supported.', $this->sourceFormat);

        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString($needle, $output);
    }

}