<?php
declare(strict_types=1);

namespace App\Commands;

use App\Task\FilteredOfferCollection;
use App\Task\OfferCollection;
use App\Task\Reader\Factory;
use Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:task', description: 'JSON based data counter')]
class CountByCommand extends Command implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private string $source = __DIR__ . '/../../data/MOCK_DATA.json';
    private string $sourceFormat = 'json';
    private string $countBy = '';
    private string $valueFrom = '';
    private string|null $valueTo = null;

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this
            ->addArgument(
                'count-by',
                InputArgument::REQUIRED,
                'Pass JSON key to count by. E.g. "VendorId"',
            )
            ->addArgument(
                'value-from',
                InputArgument::REQUIRED,
                'Value "from" to count by',
            )
            ->addArgument(
                'value-to',
                InputArgument::OPTIONAL,
                'Value "to" to count by, this parameter is optional',
            )
            ->addOption(
                'source',
                null,
                InputOption::VALUE_REQUIRED,
                'Pass file source to read data from external sources or files. E.g. "--source=/home/user/academy-task/data/MOCK_DATA.json"'
            )
            ->addOption(
                'source-format',
                null,
                InputOption::VALUE_REQUIRED,
                'Pass file type to read data from external sources or files. Available data formats JSON, XML, CSV. E.g. "--source-format=json"'
            );
    }

    /**
     * @param string|null $source
     * @param OutputInterface $output
     * @return int
     */
    private function validateOptions(InputInterface $input, OutputInterface $output): ?int
    {
        $source = $input->getOption('source');
        $sourceFormat = $input->getOption('source-format');
        if (empty($source)) {
            $output->writeln([
                '<comment>Output data will be generated from local example file',
                'To use other source use options: <info>--source</info> and <info>--source-format</info> together',
                'For more info use <info>--help</info></comment>',
                '',
            ]);
        }

        if (isset($source) && !isset($sourceFormat)) {

            $output->writeln([
                '<error>Please pass <info> --source-format </info> option</error>',
                '<comment>Only <info> --source </info>option was given</comment>',
            ]);

            return Command::FAILURE;

        }

        if (!isset($source) && isset($sourceFormat)) {

            $output->writeln([
                '<error>Please pass <info> --source </info>option</error>',
                '<comment>Only <info> --source-format </info>option was given</comment>',
            ]);

            return Command::FAILURE;
        }

        if (isset($source, $sourceFormat)) {
            try {
                file_get_contents($source);
            }
            catch (Exception $e) {
                $output->writeln([
                    sprintf('File path or URL <error>\'%s\'</error> invalid.', $source),
                ]);
                return Command::FAILURE;
            }

            if (!in_array(strtolower($sourceFormat), Factory::SUPPORTED_FORMATS)) {
                $output->writeln([
                    sprintf('Format <error>\'%s\'</error> not supported.', $sourceFormat),
                    '=====',
                    "",
                    'Supported formats: <info>JSON</info>, <info>XML</info>, <info>CSV</info>',
                    '=====',
                    '<comment>option example: </comment><info>--source=csv=/home/user/academy-task/data/MOCK_DATA.csv</info>'
                ]);

                return Command::FAILURE;
            }
            $this->sourceFormat = $sourceFormat;
            $this->source = $source;
            return Command::SUCCESS;
        }


        return Command::SUCCESS;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     */
    private function validateArguments(InputInterface $input, OutputInterface $output): ?int
    {
        $this->countBy = $input->getArgument('count-by');

        $valueFrom = $input->getArgument('value-from');
        $valueTo = $input->getArgument('value-to');

        if (isset($valueFrom) && !is_numeric($valueFrom)) {
            $output->writeln([
                sprintf('<info>Value-from</info> should be <comment>int</comment>, <error>\'%s\'</error> given.', $valueFrom),
                '=====',
            ]);
            return Command::FAILURE;
        }

        if (isset($valueTo) && !is_numeric($valueTo)) {
            $output->writeln([
                sprintf('<info>Value-to</info> should be <comment>int</comment>, <error>\'%s\'</error> given.', $valueTo),
                '=====',
            ]);
            return Command::FAILURE;
        }

        $this->valueFrom = $input->getArgument('value-from');
        $this->valueTo = $input->getArgument('value-to');

        return Command::SUCCESS;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        if ($this->validateArguments($input, $output) === Command::FAILURE) {
            return Command::FAILURE;
        }

        if ($this->validateOptions($input, $output) === Command::FAILURE) {
            return Command::FAILURE;
        }

        $factory = new Factory();
        $reader = $factory->create(format: $this->sourceFormat);

        /** @var OfferCollection $collection */
        $collection = $reader->read($this->source);

        $filtered = new FilteredOfferCollection($this->countBy, $this->valueFrom, $this->valueTo, $collection);

        $output->writeln((string)$filtered->count());

        return Command::SUCCESS;
    }

}