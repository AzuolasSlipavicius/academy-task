<?php
declare(strict_types=1);

namespace App\Task;

use Countable;
use Iterator;
use IteratorAggregate;

/**
 * Interface for The Collection class that contains Offers
 *
 * @implements IteratorAggregate<int, OfferInterface>
 */
interface OfferCollectionInterface extends IteratorAggregate, Countable
{
    public function get(int $index): OfferInterface;

    /**
     * @return Iterator<OfferInterface>
     */
    public function getIterator(): Iterator;
}