<?php
declare(strict_types=1);

namespace App\Task;

/**
 * Interface of Data Transfer Object, that represents external JSON data
 */
interface OfferInterface
{
    public function getVendorId(): int;
    public function getOfferId(): int;
    public function getKey(string $key) : string|int|float|bool|null;
}