<?php
declare(strict_types=1);

namespace App\Task;

class Offer implements OfferInterface
{
    private $data;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getVendorId(): int
    {
        return (int)$this->getKey('vendorId');
    }

    public function getOfferId(): int
    {
        return (int)$this->getKey('offerId');
    }

    public function getKey(string $key) : string|int|float|bool|null
    {
        return $this->data[$key] ?? null;
    }
}