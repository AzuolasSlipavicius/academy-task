<?php
declare(strict_types=1);

namespace App\Task\Reader;

use InvalidArgumentException;

class Factory
{
    public const SUPPORTED_FORMATS = ['json', 'xml', 'csv'];

    public function create(string $format = 'json'): ReaderInterface
    {
        $this->ensureFormat($format);
        return match (strtolower($format)) {
            'xml' => new XmlReader(),
            'csv' => new CsvReader(),
            default => new JsonReader(),
        };
    }

    /**
     * @param string $format
     * @throws InvalidArgumentException
     */
    private function ensureFormat(string $format): void
    {
        if (!in_array(strtolower($format), self::SUPPORTED_FORMATS)) {
            throw new InvalidArgumentException(sprintf('Format \'%s\' not supported.', $format));
        }
    }
}