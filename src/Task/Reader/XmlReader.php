<?php
declare(strict_types=1);

namespace App\Task\Reader;

use App\Task\OfferCollection;
use App\Task\OfferCollectionInterface;
use JsonException;

class XmlReader implements ReaderInterface
{
    /**
     * @param string $input
     * @return OfferCollectionInterface
     * @throws JsonException
     */
    public function read(string $input): OfferCollectionInterface
    {
        $content = file_get_contents($input);

        $xml = simplexml_load_string($content);
        $content = json_encode($xml, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);

        $data = json_decode($content, true, 256, JSON_THROW_ON_ERROR);

        return new OfferCollection($data['record']);
    }
}