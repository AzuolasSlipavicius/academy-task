<?php
declare(strict_types=1);

namespace App\Task\Reader;

use App\Task\OfferCollection;
use App\Task\OfferCollectionInterface;
use JsonException;

class JsonReader implements ReaderInterface
{
    /**
     * @param string $input
     * @return OfferCollectionInterface
     * @throws JsonException
     */
    public function read(string $input): OfferCollectionInterface
    {
        $content = file_get_contents($input, true);
        $data = json_decode($content, true, 256, JSON_THROW_ON_ERROR);

        return new OfferCollection($data);
    }
}