<?php
declare(strict_types=1);

namespace App\Task\Reader;

use App\Task\OfferCollection;
use App\Task\OfferCollectionInterface;
use JsonException;

class CsvReader implements ReaderInterface
{
    /**
     * @param string $input
     * @return OfferCollectionInterface
     * @throws JsonException
     */
    public function read(string $input): OfferCollectionInterface
    {
        $column_name = [];
        $final_data = [];

        $file_data = file_get_contents($input);
        $data_array = array_map("str_getcsv", explode("\n", $file_data));
        $labels = array_shift($data_array);

        foreach ($labels as $label) {
            $column_name[] = $label;
        }

        $count = count($data_array) - 1;

        for ($j = 0; $j < $count; $j++) {
            $data = array_combine($column_name, $data_array[$j]);
            $final_data[$j] = $data;
        }

        $content = json_encode($final_data, JSON_THROW_ON_ERROR);
        $data = json_decode($content, true, 256, JSON_THROW_ON_ERROR);

        return new OfferCollection($data);
    }
}