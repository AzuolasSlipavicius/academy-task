<?php
declare(strict_types=1);

namespace App\Task;

use Iterator;

class FilteredOfferCollection implements OfferCollectionInterface
{
    private OfferCollectionInterface $decorated;
    private string $key;
    private string|int|bool|float $valueFrom;
    private string|int|bool|float|null $valueTo;

    public function __construct(string $key, int|string|float|bool $valueFrom, int|string|float|bool|null $valueTo = null, OfferCollectionInterface $decorated)
    {
        $this->decorated = $decorated;
        $this->key = $key;
        $this->valueFrom = $valueFrom;
        $this->valueTo = $valueTo;
    }

    public function get(int $index): OfferInterface
    {
        return $this->decorated->get($index);
    }

    /** @inheritDoc */
    public function getIterator(): Iterator
    {
        /** @var OfferInterface $offer */
        foreach ($this->decorated as $offer) {

            // This should work if given price_from and price_to.
            if (isset($this->valueTo) && $offer->getKey($this->key) >= $this->valueFrom && $offer->getKey($this->key) <= $this->valueTo) {
                yield $offer;
            }
            elseif ($offer->getKey($this->key) == $this->valueFrom) {
                yield $offer;
            }
        }
    }

    public function count(): int
    {
        $i = 0;
        foreach ($this->getIterator() as $element) {
            $i++;
        }

        return $i;
    }
}