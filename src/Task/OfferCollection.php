<?php
declare(strict_types=1);

namespace App\Task;

use Iterator;

class OfferCollection implements OfferCollectionInterface
{

    private $data;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function get(int $index): OfferInterface
    {
        return new Offer($this->data[$index]);
    }

    /** @inheritDoc */
    public function getIterator(): Iterator
    {
        foreach ($this->data as $element) {
            yield new Offer($element);
        }
    }

    public function count(): int
    {
        return count($this->data);
    }
}